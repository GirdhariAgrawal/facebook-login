var Winston = require('winston');

//logger
var logger = new Winston.Logger({
	level: 'verbose',
	transports: [
		new Winston.transports.File({
			// name: 'info-file',
			// level: 'info',
			filename: 'loginfacebookinfo.log',
			timestamp: true
		})
		// ,
		// new Winston.transports.File({
		// 	name: 'error-file',
		// 	level: 'error',
		// 	filename: 'loginfacebookerror.log',
		// 	timestamp: true
		// })
	]
});


module.exports = logger;