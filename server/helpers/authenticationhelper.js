var jwtHelper = require('../helpers/jwtHelper');

module.exports = {
	//verify the token
	authenticate: function(req, res, next) {
		var token = req.headers.token;
		if(jwtHelper.verifyToken(token)) {
			next();
		} else
			res.send(403);
	},
	getUser: function(req) {

	},
}