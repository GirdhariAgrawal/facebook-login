var jwt = require('jsonwebtoken');
var config = require('../conf/config');

var getToken = function (value) {
	return jwt.sign({
	  exp: Math.floor(Date.now() / 1000) + (60 * 60),
	  data: value
	}, config.JWTSECRET);
}

var verifyToken = function (token) {
	var valid = false;
	
	try {
		valid = jwt.verify(token, config.JWTSECRET);
		return valid;
	} catch(err) {
		return valid;
	}
}
module.exports = {
	getToken: getToken,
	verifyToken: verifyToken
};