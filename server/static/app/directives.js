//update title directive to update title using config route
(function() {
	angular.module('facebook-login')
		
	.directive('updateTitle', updateTitle)

	updateTitle.$inject = ['$rootScope', '$timeout'];

	function updateTitle($rootScope, $timeout) {
    return {
      link: function(scope, element) {

        var listener = function(event, toState) {

        	var title = 'Facebook Login App'; //default title
        	if (toState.data && toState.data.pageTitle) title = toState.data.pageTitle;
          	element.text(title);
        };

        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
	};
})();