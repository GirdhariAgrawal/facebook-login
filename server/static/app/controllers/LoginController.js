(function() {
	angular.module('facebook-login')

	.controller('LoginController', LoginController);

	LoginController.$inject = ['$scope', '$http', '$window', '$localStorage', 'LoginService', '$state', 'AuthService'];

	function LoginController($scope, $http, $window, $localStorage, LoginService, $state, AuthService) {
		var url =	"";
		var childWindow = null;
		
		//if already loggedin go to dashboard
		var tokenValid = false;

		//is token valid or not
		AuthService.isTokenValid()
		.then(function(response) {
			tokenValid = response.data;

			if(($localStorage.token !== undefined && $localStorage.token.code !== undefined) && tokenValid) {
				$state.go('dashboard');
			}
		}, function(err) {

		});
		

		LoginService.getLoginUrl()
		.then(function(response) {
			url = response.data;
		});

		$scope.login = function() {
			childWindow = $window.open(url, "Please Login", "width:500px, height: 500px");
		}

		window.onmessage = function(e) {
			childWindow.close();
			if(e.data) {
				$localStorage.token = e.data;
				$state.go('dashboard');
			}
		}
	}
})();