(function() {
	angular.module('facebook-login')

	.controller('DashboardController', DashboardController);

	DashboardController.$inject = ['$scope', 'UserService'];

	function DashboardController($scope, UserService) {
		
		UserService.getUserDetails()
		.then(function(response) {
			$scope.user = response.data;
		}, function(err) {

		});
	}
})();