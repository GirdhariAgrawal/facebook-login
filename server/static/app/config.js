//configuration object

(function() {
	angular.module('facebook-login').config(Config);

	Config.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
	
	/**
	 * Configuration of application
	 */
	function Config($stateProvider, $urlRouterProvider, $httpProvider) {

		$urlRouterProvider.otherwise('/');

		$stateProvider

			//home state i.e. login
			.state('home', {
				url: '/',
				templateUrl: 'templates/login.html',
				controller: 'LoginController',
				data: {
					pageTitle: 'Login | Sample Application'
				}
			})

			// dashboard
			.state('dashboard', {
				url: '/me',
				templateUrl: 'templates/dashboard.html',
				controller: 'DashboardController',
				data: {
					pageTitle: 'Welcome User',
				}
			})
			
			//logout user
			.state('logout', {
				url: '/logout',
				controller: function($state, $localStorage) {
					delete $localStorage.token;
					$state.go('home');
				},
			})

		//Create Http interceptor
		$httpProvider.interceptors.push(['$q', '$localStorage', '$rootScope', '$injector', function($q, $localStorage, $rootScope, $injector) {
			return {
				'request': function (config) {
					config.headers = config.headers || {};
					if ($localStorage.token) {
						config.headers.Token = $localStorage.token.code;
					}
					return config;
				},
				'responseError': function(response) {
					if(response.status === 401 || response.status === 403 | response.status == 404) {
						delete $localStorage.token;
						$injector.get('$state').transitionTo('home');
						$rootScope.$emit("unauthorized");
					}
					return $q.reject(response);
				}
			};
		}]);
	}
})();