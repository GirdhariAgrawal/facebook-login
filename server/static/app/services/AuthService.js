(function() {
	angular.module('facebook-login')

	.factory('AuthService', AuthService);

	AuthService.$inject = ['$http'];

	function AuthService($http) {
		var service = this;

		//decode token using api to verify if token is valid
		service.isTokenValid = function() {
			return $http({
				url: '/user/tokenvalid',
				method: 'post'
			});
		}

		return service;
	}
})();