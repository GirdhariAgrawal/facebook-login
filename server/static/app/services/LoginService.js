(function() {
	angular.module('facebook-login')

	.factory('LoginService', LoginService);

	LoginService.$inject = ['$http'];

	function LoginService($http) {
		var service = this;

		//get login url
		service.getLoginUrl = function() {
			return $http({
				url: '/facebook/login',
				method: 'get'
			});
		}
		return service;
	}
})();