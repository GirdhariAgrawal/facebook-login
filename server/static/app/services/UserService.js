(function() {
	angular.module('facebook-login')

	.factory('UserService', UserService);

	UserService.$inject = ['$http'];

	function UserService($http) {
		var userService = this;

		//call user service to get user details
		userService.getUserDetails = function() {
			return $http({
				url: './user/getuser',
				method: 'post'
			});
		}

		return userService;
	}

})();