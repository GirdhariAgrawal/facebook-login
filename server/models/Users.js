/**
 * Model for User
 */
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema({
	email: {type: String, required: true, unique: true},
	fullname: {type: String, required: true},
	loginCount: {type: Number, required: true, default: 0},
	created_at: {type: Date, default: Date.now()},
	token: {type: String, required: true},
	updated_at: Date,
	imagepath: {type: String, required: true}
});

User = mongoose.model('User', userSchema);

module.exports = User;