var express = require('express');
var path = require('path');
var router = express.Router();
var request = require('request');
var config = require('../conf/config');
var jwtHelper = require('../helpers/jwtHelper');
var user = require('../models/Users');

//facebook callback url
router.get('/callback', function(req, res) {
	res.sendFile(path.resolve(__dirname, '..', 'views', 'token.html'));
});

//get bearer token for future request
router.get('/token', function(req, res) {
	
	var url = "https://graph.facebook.com/v2.8/oauth/access_token?client_id="+config.FACEBBOKAPPID+"&redirect_uri="+config.CALLBACKURL+"&client_secret="+config.FACEBOOKAPPSECRET+"&code="+req.query.code;
	
	request(url, function (error, response, body) {
		
		var access = JSON.parse(body).access_token;
	  var accessToken = "https://graph.facebook.com/me?fields=id,name,picture,email&access_token="+access;
	  
	  request(accessToken, function(error, response, body) {
	  	//jwt bearer token
	  	var token = jwtHelper.getToken(body);
	  	
	  	var userData = JSON.parse(body);

	  	User.findOne({email: userData.email}, function(err, user) {
	  		if(!user) {
	  			var user = new User({
			  		email: userData.email,
			  		fullname: userData.name,
			  		loginCount: 1,
			  		token: token,
			  		imagepath: userData.picture.data.url
			  	});

			  	user.save(function(err, user) {
			  		if(err) {
			  			res.send(400);
			  		}
			  	});
	  		} else {
	  			token = jwtHelper.getToken(body);
	  			User.findOneAndUpdate({email : userData.email}, { token:  token, $inc: { loginCount: 1 }}, function (err, user) {
						if(err) res.send(400);
					});
	  		}
			  res.send(token);
	  	});
	  });
	});
});

//client facebook login url content
router.get('/login', function(req, res) {
	var url = "https://www.facebook.com/v2.8/dialog/oauth?client_id=1864984640410842&redirect_uri="+config.CALLBACKURL+"&scope=email";
	res.send(url);
});

module.exports = router;