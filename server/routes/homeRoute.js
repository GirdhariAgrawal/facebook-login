var express = require('express');
var router = express.Router();
var path = require('path');
var app = express.Router();

router.get('/', function(req, res) {
	res.sendFile(path.resolve(__dirname, '..','views', 'index.html'));
});

module.exports = router;