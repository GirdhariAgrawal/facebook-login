var router = require('express').Router();
var path = require('path');
var auth = require('../helpers/authenticationhelper');
var jwtHelper = require('../helpers/jwtHelper');

router.get('/', function(req, res) {
	res.sendFile(path.resolve(__dirname, '..','views', 'dashboard.html'));
});

router.post('/getuser', auth.authenticate, function(req, res) {

	//get user detail as per token
	User.findOne({token : req.headers.token}, function (err, user) {
			if(!user) {
				res.send(403);
			} else {
				res.send(user);
			}
	});
});

router.post('/tokenvalid', function(req, res) {
	res.send(jwtHelper.verifyToken(req.headers.token));
});

module.exports = router;