var express = require("express");
var mongoose = require('mongoose');
var path = require("path");
var favicon = require("serve-favicon");
var Winston = require("winston");
var http = require('http');
var path = require('path');

var app = express();

var logger = require('./helpers/logger');

var config = require('./conf/config');

var PORT = config.PORT;

mongoose.connect(config.MONGODB);

//static file
app.use(express.static(path.join(__dirname, 'static')));

//route handlers
var homeRoute = require('./routes/homeRoute');
var userRoute = require('./routes/userRoute');
var facebook = require('./routes/facebook');

app.use('/user', userRoute);
app.use('/facebook', facebook);
app.use('/', homeRoute);

mongoose.connection.on('error', function(err, data) {
	logger.error('Connection with mongodb failed');
});

mongoose.connection.once('open', function(err, data) {
	logger.info('Connected with mongodb');
});

app.listen(PORT);
logger.info('Server is running on ', {port: PORT});